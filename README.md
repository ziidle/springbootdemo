# For testing only
Java app utilizing Spring Boot and MongoDB. Git repository managed in Bitbucket using their pipeline for CI and CD. Build, dependency, and testing executed by Gradle.

Deployed to a kubernetes cluster

##Lifecycle:
* Push changes feature branch
    * ./gradlew build runs which will report failures to unit tests
* Pull request to master 
    * Not currently configured but would deploy to QA environment -- currently runs ./gradlew build
* Pull request to release
    * Runs ./gradlew build dockerBuild with builds, tests, and if tests are successful, updates the docker image.
    * Pipeline then updates kubernetes deployment image pushing changes to production.
* Both QA and Production environments monitored by ELK stack on kubernetes
    * While I setup the stack and tested deployment at kibana.ziidle.com:5601 I didn't have time to connect it in any meaningful way to the app.