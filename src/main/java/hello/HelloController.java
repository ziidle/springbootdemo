package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@RestController
public class HelloController {

    @Autowired
    private CustomerRepository repository;
    
    @RequestMapping("/")
    public String index() {
        return "Greetings from space!!";
    }
    
    @RequestMapping("/ping")
    public String ping() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(repository.findAll());
    }

}